package com.shaanstraining.ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

public class HomePage extends BasePage {
	
	public void search(RemoteWebDriver driver, String searchTerm) {
		WebElement searchInputField = driver.findElement(By.name("q"));
		searchInputField.sendKeys(searchTerm);
		searchInputField.submit();
	}
}