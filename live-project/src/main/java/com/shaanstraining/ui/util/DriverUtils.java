package com.shaanstraining.ui.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
public class DriverUtils {
	
	private static Properties props;
	private static final String DRIVER_PROP_FILE = "src/main/resources/driver.properties";
	
	public static RemoteWebDriver getDriver(RemoteWebDriver driver, String browser, String baseURL) throws IOException, FileNotFoundException, Exception {
		props = new Properties();
		props.load(new FileInputStream(DRIVER_PROP_FILE));
		
		if(isWindows()) {
			if(browser.equalsIgnoreCase("firefox")) {
				FirefoxOptions options = new FirefoxOptions();
				options.setCapability(CapabilityType.BROWSER_VERSION, 66);
				
				System.setProperty("webdriver.gecko.driver", props.getProperty(Constants.FIREFOX_DRIVER_WIN));
				driver = new FirefoxDriver();
			}
			if(browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", props.getProperty(Constants.CHROME_DRIVER_WIN));
				driver = new ChromeDriver();
			}
		}
		driver.get(baseURL);
		
		return driver;
	}
	
	public static RemoteWebDriver getDriver(RemoteWebDriver driver, String hub, String browser, String baseUrl) throws IOException,
	FileNotFoundException, Exception {
				
		//to run using Selenium Grid
//		if(browser.equalsIgnoreCase("firefox")) {
//			FirefoxOptions options = new FirefoxOptions();
//			driver = new RemoteWebDriver(new URL(hub), options);
//		}
//		if(browser.equalsIgnoreCase("chrome")) {
//			ChromeOptions options = new ChromeOptions();
//			driver = new RemoteWebDriver(new URL(hub), options);
//		}
		
		//To run the tests using SauceLabs
		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
		
		desiredCapabilities.setBrowserName("Chrome");
		desiredCapabilities.setVersion("71.0");
		desiredCapabilities.setCapability(CapabilityType.PLATFORM, "Windows 10");
		desiredCapabilities.setCapability("username", "vinhaquite");
		desiredCapabilities.setCapability("accessKey", "5cb5290e-a4b8-4dc1-a8a8-90dd614e696e");
		desiredCapabilities.setCapability("name", "SDET Training - UI Test");
//		
		//To run the tests using SauceLabs from Jenkins
//		desiredCapabilities.setBrowserName(System.getenv("SELENIUM_BROWSER"));
//		desiredCapabilities.setVersion(System.getenv("SELENIUM_VERSION"));
//		desiredCapabilities.setCapability(CapabilityType.PLATFORM, System.getenv("SELENIUM_PLATFORM"));
//		desiredCapabilities.setCapability("username", System.getenv("SAUCE_USERNAME"));
//		desiredCapabilities.setCapability("accessKey", System.getenv("SAUCE_ACCESS_KEY"));
//		desiredCapabilities.setCapability("name", "SDET Training - UI Test");
//		
		driver = new RemoteWebDriver(new URL(hub), desiredCapabilities);
		driver.get(baseUrl);
		
		return driver;
	}
	
	private static boolean isWindows() {
		String os = System.getProperty("os.name");
		return os.startsWith("Windows");
	}
}
