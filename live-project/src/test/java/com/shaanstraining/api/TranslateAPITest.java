package com.shaanstraining.api;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;

import java.io.FileReader;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.testng.annotations.Test;

public class TranslateAPITest {
	static final String JSON_FILE = "src/test/resources/translate-api-positive.json";
	static final String JSON_FILE_2 = "src/test/resources/translate-api-negative.json";
	
	@Test(enabled=false)
	public void translateTest() throws Exception {
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(new FileReader(JSON_FILE));
		
		JSONObject jsonObject = (JSONObject) obj;
		
		given().
			header("contentType", "application/json").and().
			body(jsonObject.toJSONString()).
		when().
			post("https://translation.googleapis.com/language/translate/v2?key=AIzaSyD1ktJSjROx50db6zRP-iFPPBjxXcJCHx8").
		then().
			statusCode(200).
			log().all();
	}
	
	@Test
	public void translateAPINegativeTest_InvalidTargetLanguage() throws Exception {
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(new FileReader(JSON_FILE_2));
		
		JSONObject jsonObject = (JSONObject) obj;
		
		given().
			header("contentType", "application/json").and().
			body(jsonObject.toJSONString()).
		when().
			post("https://translation.googleapis.com/language/translate/v2?key=AIzaSyD1ktJSjROx50db6zRP-iFPPBjxXcJCHx8").
		then().
			statusCode(400).
			log().all();
	}
	
	@Test
	public void translateAPINegativeTest_ExpiredAPIKey() throws Exception {
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(new FileReader(JSON_FILE));
		
		JSONObject jsonObject = (JSONObject) obj;
		
		given().
			header("contentType", "application/json").and().
			body(jsonObject.toJSONString()).
		when().
			post("https://translation.googleapis.com/language/translate/v2?key=AIzaSyD1ktJSjROx50db6zRP-iFPPBjxXcJCHx8").
		then().
			statusCode(400).
			body("error.message", containsString("API key expired. Please renew the API key.")).
			log().all();
	}
	
	@Test
	public void translateNegativeTest_InvalidAPIKey() throws Exception {
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(new FileReader(JSON_FILE));
		
		JSONObject jsonObject = (JSONObject) obj;
		
		given().
			header("contentType", "application/json").and().
			body(jsonObject.toJSONString()).
		when().
			post("https://translation.googleapis.com/language/translate/v2?key=AIzaSyD1ktJSjROx50db6zRP-invalidkey").
		then().
			statusCode(400).
			body("error.message", containsString("API key not valid. Please pass a valid API key.")).
			log().all();
	}
}
